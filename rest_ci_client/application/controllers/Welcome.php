<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->helper(array('form', 'url', 'file')); // fungsi mempersingkat pembuatan form
		$this->load->model('MyModel'); // untuk memanggil file model [mpertama adlh nama model]
		$this->load->library('upload','session', 'database');//. fungsi mengupload file
	}
	
	public function index()
	{
		$this->load->view('welcome_message');
	}

	public function ambilsales(){
		$data = $this->MyModel->getsales();
		echo json_encode($data);
	}
}
